var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  walker: 255
}

var boardSize
var walker = [0, 0, 0, 10]
var walkerHistory = []
walkerHistory.push(walker)
var minX, minY, minZ, maxX, maxY, maxZ
var transitionZ = 0
var rotation = 0

var frames = 0
var cam

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight, WEBGL)
  cam = createCamera()
  for (var i = 0; i < 1024; i++) {
    var moveX = -20 + Math.random() * 40
    var moveY = -20 + Math.random() * 40
    var moveZ = -20 + Math.random() * 40
    var newSize = 4 + Math.random() * 12
    walker = [walker[0] + moveX, walker[1] + moveY, walker[2] + moveZ, newSize]
    walkerHistory.push(walker)

    minX = getMin(walkerHistory, 0)
    minY = getMin(walkerHistory, 1)
    minZ = getMin(walkerHistory, 2)
    maxX = getMax(walkerHistory, 0)
    maxY = getMax(walkerHistory, 1)
    maxZ = getMax(walkerHistory, 2)
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(0)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  cam.move(0, 0, transitionZ)
  cam.move(0, 0, -0.5 * (minX + minY))
  lights()

  push()
  rotateY(0.01 * rotation)
  rotateX(0.005 * rotation)
  push()
  translate(-(minX + maxX) * 0.5, -(minY + maxY) * 0.5, 0)
  if (walkerHistory.length > 1) {
    for (var i = 0; i < walkerHistory.length - 1; i++) {
      noFill()
      stroke(colors.walker)
      strokeWeight(4)
      line(walkerHistory[i][0], walkerHistory[i][1], walkerHistory[i][2], walkerHistory[i + 1][0], walkerHistory[i + 1][1], walkerHistory[i + 1][2])
    }
  }
  for (var i = 0; i < walkerHistory.length; i++) {
    push()
    translate(walkerHistory[i][0], walkerHistory[i][1], walkerHistory[i][2])
    fill(colors.walker)
    noStroke()
    sphere(walkerHistory[i][3])
    pop()
  }
  pop()
  pop()

  frames += deltaTime * 0.025
  rotation += deltaTime * 0.01
  if (frames > 960) {
    frames = 0

    walker = [0, 0, 0, 10]
    walkerHistory = []
    transitionZ = 0
    for (var i = 0; i < 1024; i++) {
      var moveX = -20 + Math.random() * 40
      var moveY = -20 + Math.random() * 40
      var moveZ = -20 + Math.random() * 40
      var newSize = 4 + Math.random() * 12
      walker = [walker[0] + moveX, walker[1] + moveY, walker[2] + moveZ, newSize]
      walkerHistory.push(walker)

      minX = getMin(walkerHistory, 0)
      minY = getMin(walkerHistory, 1)
      minZ = getMin(walkerHistory, 2)
      maxX = getMax(walkerHistory, 0)
      maxY = getMax(walkerHistory, 1)
      maxZ = getMax(walkerHistory, 2)
    }
  }
}

function mouseWheel(event) {
  transitionZ += event.delta * 100
}

function mousePressed() {
  walker = [0, 0, 0, 10]
  walkerHistory = []
  transitionZ = 0
  for (var i = 0; i < 1024; i++) {
    var moveX = -20 + Math.random() * 40
    var moveY = -20 + Math.random() * 40
    var moveZ = -20 + Math.random() * 40
    var newSize = 4 + Math.random() * 12
    walker = [walker[0] + moveX, walker[1] + moveY, walker[2] + moveZ, newSize]
    walkerHistory.push(walker)

    minX = getMin(walkerHistory, 0)
    minY = getMin(walkerHistory, 1)
    minZ = getMin(walkerHistory, 2)
    maxX = getMax(walkerHistory, 0)
    maxY = getMax(walkerHistory, 1)
    maxZ = getMax(walkerHistory, 2)
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight, WEBGL)
}

function getMin(array, axis) {
  var coords = []
  for (var i = 0; i < array.length; i++) {
    coords.push(array[i][axis])
  }
  var min = Math.min(...coords)
  return min
}

function getMax(array, axis) {
  var coords = []
  for (var i = 0; i < array.length; i++) {
    coords.push(array[i][axis])
  }
  var max = Math.max(...coords)
  return max
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
